#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator('female')
	say('Seraphine','Greetings! My name is '+name+'. I am','your personal animal care attendant.')

say(name,'I take care of your pets. Do you want','to know more?')
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'Pets hatch from eggs which can be','found in dungeons.')
	say(name,'You can own up to 6 pets that will','follow you on your adventures.')
	say(name,'They can be very helpful in battles.',' ')
	say(name,'If you travel together with your pet','your relationship will grow.')
	say(name,'When the relationship is strong enough','your pet will evolve and become')
	say(name,'stronger.',' ')

if first_meet == True:
	help_con = container([il.ilist['misc'][53]])
	say(name,'By the way I have a little present','for you.')
	help_con.loot(0)
	say(name+' hands over a camera',' ',' ')
	say(name,'This is a camera. You can use it to','take screenshots.')
	say(name,'This way you can keep all the good','moments with your pets forever.')
	say(name,'I hope you will enjoy this gift.','')
	player.quest_variables.append('met_petkeeper')
	first_meet = False

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
