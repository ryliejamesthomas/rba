#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator('female')
	say('Seraphine','Hello! I\'m '+name+'. I protect','this elysium with my magic.')

say(name,'I can teach you how to fight monsters.','...')
say(name,'With the power of MAGIC!','Not like an idiot with a sword.')
say(name,'You are interested, aren\'t you?',' ')
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'Magic attacks are ranged attacks.','Cool eh?...')
	say(name,'To use magic you need to equip a','magic weapon in your left hand.')
	say(name,'Then you can press ['+key_name['f']+'] and','choose a direction with ['+key_name['wasd']+'].')
	say(name,'If your power is high enough and','you use a good magic weapon you\'ll hit.')
	say(name,'Your will can protect you from magical','damage. But an amulet or a')
	say(name,'necklace can be helpful too.','Isn\'t magic awesome?')
if first_meet == True:
	say(name,'Wait a second!',' ')
	say(name,'I\'ve put somethin in your chest!',' ')
	say(name,'It is a original wand!','Not the strongest weapon but perfect')
	say(name,'for a beginner like you.','Have fun!')
	player.quest_variables.append('met_mage')
	first_meet = False

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
