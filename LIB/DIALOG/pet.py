#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	say('A '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' has hached!',' ','What do you like to do?')
	ui = ask('Keep it!','Dismiss it!',None)
	if ui == 1:
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name = screen.string_input('Give your new pet a name!',15)
		first_meet = False
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = 'first_meet = False;'
	else:
		message.add('The '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' has gone forever!')
		screen.write_hit_matrix(mob_x,mob_y,7)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		
if first_meet == False:
	if player.pet_pos != [mob_x,mob_y,mob_z] or player.pet_on_map != mob_on_map:
		if player.pet_pos != False and player.pet_on_map != False:
			test = pet_return(player.pet_pos[0],player.pet_pos[1],player.pet_pos[2],player.pet_on_map)
			if test == False:
				message.add('You can\'t adopt one more pet!')
		player.pet_pos = [mob_x,mob_y,mob_z]
		player.pet_on_map = mob_on_map
		player.pet_lp = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].basic_attribute.max_lp
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].AI_style = 'company'
		message.add(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' follows you now.')
	elif player.pet_pos == [mob_x,mob_y,mob_z] and player.pet_on_map == mob_on_map:
		if player.buffs.get_buff('immobilized') == 0:
			swap_places(mob_x,mob_y,mob_z,mob_on_map)
