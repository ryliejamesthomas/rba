#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator()
	say('Seraph','Greetings! My name is '+name+'.','I am the treasurer of this place.')

say(name,'I take care for the items you send','home. Do you want to know more?')
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'To send an item home press ['+key_name['e']+'].','Next choose "manage" and then')
	say(name,'"send home".','We can store up to 7 items of every')
	say(name,'category (Equipment,Food,Misc)','at this elysium.')

if first_meet == True:
	help_con = container([il.ilist['misc'][51]])
	say(name,'...',' ')
	say(name,'Oh... I almost forgot!','I have something for you.')
	help_con.loot(0)
	say(name+' hands over some chalk',' ',' ')
	say(name,'This is magic chalk. You can use it to','write a magic word on the floor.')
	say(name,'Monsters can not pass this sign. This','can help you to escape dangerous')
	say(name,'situations.','But be careful! It will break if')
	say(name,'you use it to often.',' ')
	player.quest_variables.append('met_treasurer')
	first_meet = False

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
