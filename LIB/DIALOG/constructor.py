#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator()
	say('Seraph','Good day! My name is '+name+'.','I have built this elysium.')
	say(name,'Isn\'t it beautiful?',' ')
	say(name,'Of course your personal room is a bit','small...')
	say(name,'But with the right tools I would be','able to change this.')
	say(name,'Just come to me if you find something','suitable.')

say(name,'I can tell you how to build stuff.','Are you interested?')

ui = ask('No','Yes',None)
if ui == 2:
	say(name,'First you need some resources.','Most things are constructed from')
	say(name,'wood and stone, but for','agriculture you\'ll need some seeds.')
	say(name,'If you\'d like to start building stuff','press ['+key_name['b']+'].')
	say(name,'Next you can choose what to build.','Finally you can pick position and')
	say(name,'size of your construction.','Press ['+key_name['e']+'] to finish your work.')

if first_meet == True:
	say(name,'If you like to build things you\'ll','need some resources.')
	say(name,'I\'ve put something in the chest','inside your room.')
	say(name,'Its an axe and a pickaxe!',' ')
	say(name,'With the pick you can break rocks','in order to obtain some stone.')
	say(name,'And the axe allows you to chop down','trees for wood.')
	say(name,'These are good tools!',' ')
	player.quest_variables.append('met_constructor')
	first_meet = False

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
