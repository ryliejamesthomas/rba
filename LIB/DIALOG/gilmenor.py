#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	say('Gilmenor','You damn monsters!','LET ME OUT!!!','gilmenor_angry')
	say('Gilmenor','...',' ','gilmenor_serious')
	say('Gilmenor','You are a wanderer just like me?!','Praise the gods!','gilmenor_think')
	say('Gilmenor','I was exploring this dungeon when','this monsters caught me.','gilmenor_think')
	say('Gilmenor','Strange things are going on!','The wizard of this dungeon turned','gilmenor_serious')
	say('Gilmenor','the inhabitants of this world to stone!',' ','gilmenor_serious')
	say('Gilmenor','But where are my manners?','I need to introduce myself!','gilmenor_happy')
	say('Gilmenor','Gilmenor','Adventurer,treasure hunter and','gilmenor_happy')
	say('Gilmenor','explorer at your service.',' ','gilmenor_happy')
	first_meet = False

say('Gilmenor','Would you help me to escape from here?',' ','gilmenor_serious')
ui = ask('No','Yes',None)
if ui == 2:
	say('Gilmenor','Thank you so much! I\'ll remember','this.','gilmenor_serious')
	say('Gilmenor','There should be a teleporter somewhere','on this floor. If we could reach it','gilmenor_serious')
	say('Gilmenor','I can escape this place.','I will follow you.','gilmenor_serious')
	if player.pet_pos != [mob_x,mob_y,mob_z] or player.pet_on_map != mob_on_map:
		if player.pet_pos != False and player.pet_on_map != False:
			test = pet_return(player.pet_pos[0],player.pet_pos[1],player.pet_pos[2],player.pet_on_map)
		else:
			test = True

		if test == True:
			player.pet_pos = [mob_x,mob_y,mob_z]
			player.pet_on_map = mob_on_map
			player.pet_lp = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].basic_attribute.max_lp
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].AI_style = 'company'
			message.add(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' follows you now.')
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].message = 'swap_place'
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].personal_id = 'gilmenor'

#now save the new memories
mem_string = 'first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
