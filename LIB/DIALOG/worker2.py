#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

messages = (
			'I\'m busy right now.',
			'Thanks for returning my tools.',
			'I hate this rats down here.',
			'Next time I will fight for my tools.',
			'You really helped me a lot!'
			)

ran = random.randint(0,len(messages)-1)
mes = 'Elfish Worker: '+messages[ran]

message.add(mes)
swap_places(mob_x,mob_y,mob_z,mob_on_map)
