#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

messages = (
			'Greetings my friend!.',
			'Free again! Feels good.',
			'I spent my whole life inside this caves.',
			'I\'m happy to feel the dirt again in my hands.',
			'I love stone. But I don\'t like to be one.'
			)

ran = random.randint(0,len(messages)-1)
mes = 'Dwarf: '+messages[ran]

message.add(mes)
swap_places(mob_x,mob_y,mob_z,mob_on_map)
